const bannersData = [

	{
		id:"banner-01",
		title: "Zuitt Coding Bootcamp",
		description: "Opportunities for everyone, everywhere.",
		destination: "/courses",
		button: "Enroll now"
	},
	{
		
		id:"banner-02",
		title: "Error 404- Page not Found!",
		description: "The page you are looking for cannot be found",
		destination: "/",
		button: "Back to Home"
		
	}


]

export default bannersData;