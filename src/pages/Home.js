import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import bannersData from '../data/bannersData';
// import CourseCard from '../components/CourseCard';

export default function Home() {

	const banners = bannersData.map(banner=> {
		return (
				<Banner key={banner.id} bannerProp={banner}/>
			)
	})

	return (

			<>
				{banners[0]}
				<Highlights />
				{/*<CourseCard />*/}
			</>




		)
}