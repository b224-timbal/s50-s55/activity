import { Link } from 'react-router-dom';
import {Row, Col, Button} from 'react-bootstrap';


export default function Banner({bannerProp}) {

	const { title, description, button, destination, id } = bannerProp;

	return (

			<Row>
				<Col className="p-5">
					<h1>{title}</h1>
					<p>{description}</p>
					<Button variant="primary" as={Link} to={destination}>{button}</Button>
					
				</Col>
			</Row>

		)
}