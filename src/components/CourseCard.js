import { useState, useEffect } from 'react';
import { Row, Col, Card, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';


export default function CourseCard({courseProp}) {
	// Checks to see if the data was successfully passed.
	// console.log(props);
	// Every components receives information in a form of object.
	// console.log(typeof props);
	// Using the dot notation, access the property to retrieve the property of data.
	// console.log(props.courseProp.name);
	// Checks if we can retrieve data from courseProp
	console.log(courseProp);

	/*
		Use the state hook for this component to be able to store its state.
		States are used to keep traack of the information related to individual components

		Syntax:
			const [getter, setter] = useState(initialGetterValue)


	*/
	const [count, setCount] = useState(0);
	// console.log(useState(0));
	const [seats, setSeats] = useState(30);

	function enroll() {
		
		/*if (seats === 0) {
			alert(`No more seats available on ${name}!`)
		} else {
			setCount(count + 1);
			setSeats(seats -1);
			console.log('Enrollees: '+ count);
			console.log('Seats: '+ seats);
		}*/

		if(count < 30) {
					setCount(count + 1)
					// console.log('Enrollee: ' + count)
					setSeats(seats - 1)
					// console.log('Seats: ' + seats)
				} //else {
					// alert("No more seats available.")
				// }
		
		
	}

	useEffect(() => {
		if(seats === 0) {
			alert("No more seats available.")
		}
	}, [seats])

	const { name, description, price, _id } = courseProp;

	return (

				<Row className="mt-3 mb-3">
					<Col xs={12} md={6}>
						<Card className="cardHighlight p-3">
						      <Card.Body>
						        <Card.Title>{name}</Card.Title>
						        <Card.Subtitle className="mb-2">Description:</Card.Subtitle>
						        <Card.Text>
						          {description}
						        </Card.Text>
						        <Card.Title>Price:</Card.Title>
						        <Card.Text>PHP {price}</Card.Text>
						        <Button className="bg-primary" as={ Link } to={`/courses/${_id}`}>Details</Button>
						      </Card.Body>
						</Card>

					</Col>
				</Row>

			)
}